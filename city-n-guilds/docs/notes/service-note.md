# On Serving Customers

## Americano vs. Filter Coffee

If a customer want to have a filter coffee but you only have an espresso machine:

- Explain to the customer that we don't have this item on the menu OR
- Suggest the customer to order **Americano** instead

The reason of suggesting Americano is that it has the **most similar** cup with filter coffee:

- No sugar
- No milk

## Communication Skills

### Serving

When you serving a customer:

- Make eye contact
- Nod **politely** while listen to customer
- You can repeat the customer's opinion in order to let the customer knows that you understand

### Billing

When a customer want to get the bill:

- Check the bill
- Inform the customers on billing **details and amount**
- Pass the bill to customer

## Handling Complaints

If you handle camplaints properly: (mrs)

- Customer might **r**eturn in the future
- Build a good **r**eputation of the shop
- Sales **r**evenue increase --> increase the profit --> salary increase --> staff **m**orale and **s**atisfication increase

If you failed to handle the complaints: (mrs)

- Customer won't **r**eturn to the shop (due to dis**s**atisfaction)
- Sales **r**evenue decrease --> staff **m**orale decrease

When handling a complaint: (us)

- Understand the problem (some problems might just a *misunderstanding*)
- Solve the problem

## Payment methods

- Cash
- Credit / Debit card
- Stored value card
- Coupon / Vouchers

## Handling Wounds

Use a **blue / obvious color** bandage:

- Not many foods are blue in color --> easy to be spotted by others