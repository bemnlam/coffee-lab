# On Coffee

## Major types of coffee beans

- 98% of coffee beans we consume are either **Arabica** (70%) and **Robusta** (28~29%).
- Coffee beans are commonly grown in the **Coffee Belt**.

## Difference between Arabica and Robusta

|                                                | Arabica                                                      | Robusta                                       |
| ---------------------------------------------- | ------------------------------------------------------------ | --------------------------------------------- |
| **A**ltitude                                   | Higher (≥ 1200m)                                             | Lower (≤ 1000m)                               |
| **C**limate                                    | Cooler (15 - 25˚C)                                           | Warmer (24 - 30˚C)                            |
| Coffee disease (**CBD**) and insect resistance | Lower                                                        | Higher                                        |
| **C**affeine content                           | Lower (~ 1 - 1.5%)                                           | Higher (~ 2% - 3%)                            |
| **C**olor (of green bean)                      | Light green                                                  | Green brown                                   |
| **S**hape                                      | Oval                                                         | Round                                         |
| **S**ize                                       | Larger                                                       | Smaller                                       |
| Taste                                          | Cleaner taste (**C**), Lighter body (**L**), Higher acidity (**S**) | Higher bitterness, Bolder body, Lower acidity |

![Arabica and Robusta](https://aromapass.com/wp-content/uploads/2017/05/Arabica-vs-Robusta-Beans.jpg)

## Harvesting

- **Selective picking** is the **best way** to get **better quality** of coffee
  - Coffee farmer can **judge** when will be the best time to harvest the ripe coffee beans from trees.
  - They won't pick the unripe / overripe / rotten coffee cherries

## Processing

Two major processing methods for coffee cherries: **washed processing** and **natural processing**.

|         | Washed         | Natural          |
| ------- | -------------- | ---------------- |
| Acidity | Higher acidity | Lower acidity    |
| Taste   | Cleaner taste  | Higher sweetness |
| Flavour | Finer flavours | Stronger flavour |

**Note:** Natural processing may yields slightly bitter coffee beans then washed processing coffee beans.

### Fairtrade

- Provides an alternative way then the conventional trade
- Partnership between coffee producers and comsumers: coffee retailers buy coffee **directly** from the coffee farmers
- Provides a better deals and improved terms of trade to the farmers: coffee farmers can improve their living and coffee planting
- Provides a simple and powerful way to the consumers to reduce poverty through daily shopping.

### Roasting

- Coffee green beans need to be roasted before brewing: heat will trigger chemical reactions (e.g. *maillard reaction*) and devleop flavours during roasting.
- Different roasting levels give the coffee beans different tastes and flavours:

![Roasting levels](https://www.drivencoffee.com/wp-content/uploads/2013/12/coffeeroastlevels.jpg)

- Drinkable starts from lvl 7: size of the beans increase after the 1st crack
- lvl 7-8 light roast; lvl 9-10 medium (filter coffee); lvl 12-13 espresso; lvl15-16 chained coffee shop
- The coffee beans are brunt when > lvl 16

| Roast level     | Lighter                        | Darker         |
| --------------- | ------------------------------ | -------------- |
| Acidity         | Higher                         | Lower          |
| Taste / flavour | Fruity / floral, Finer flavour | Stronger taste |
| Body            | Balanced                       | Bold           |

### Blending

- To present different flavours and tastes in a cup
- To develop a new flavour when blending beans
- To make a cup with good body and acidity / more balanced
- Usually mixing 2-4 kinds of beans
- Example: Barzil (nutty, choco) + Mandheling (bold body) --> good taste and good body

### Storage

#### After the Roast

Coffee beans requires ≥ 24 hours to reach the best condition after roasting. However, if the beans stay too long:

- Flavour and aroma starts to fade away
- Quality drops and worsen the taste of the cup

#### Oxidation

Coffee beans will undergo oxidation when exposing to air (oxygen) and cause

- Flavour and aroma starts to fade away
- Beans quality drops

#### Prefeable way to store roasted beans

- Using a coffee bean bag with ziploc and 1-way valve (with reflective inner coating e.g. Al coating).
  - CO<sub>2</sub> can leave the bag, preventing the bag explode due to too much gas inside
  - O<sub>2</sub> and sunlight can't enter the bag, preventing oxidation
- Keep airtight
- Keep in a dark and cool place
- Keep away from the place with strong scent
- Open the coffee bean bag only when taking the beans out
- Grind the beans for daily use only
- Grind the beans right before use

![1-way valve coffee bean bag](https://i1.wp.com/ae01.alicdn.com/kf/HTB1Wj.waECF3KVjSZJnq6znHFXaz/13x20cm-100pcs-Stand-Up-Zipper-Bags-Flat-Bottom-Colored-Aluminium-Foil-Coffee-Bean-Storage-Pouch-Valve.jpg)

## Espresso

Before brewing the expresso, make sure that you:

- Washed the **grouphead**;
- Warmed the espresso **cup**;
- Filled amd tamped the **portafilter** using correct dose of grinds.

### Appearance

- **Crema** is the essence of an espresso.
- Expected appearance: **crema** should **stretch** the whole surface without any **holes** or  **broken spots**. (no BS)
- Color of the crema: **amber** 🟠

#### If your crema looks bad...

Here are some possible causes of having a bad crema:

- The coffee beans are not **fresh**
- Coffee **grinds** is too coarse / too fine
- Boiler's **temperature** (and hence the brewing temperature) is too high
- Brew time is too **short**
- Extraction **rate** is too low

![espresso](https://img.thedailybeast.com/image/upload/v1504817894/170906-espresso-tease_x8drzw.jpg)

### Brewing Criteria

|                             | Requirements       |
| --------------------------- | ------------------ |
| (brewing) Pressure          | 8 - 9 Bars         |
| (brewing) Temperature       | 86 - 92˚C          |
| (brewing) Time              | 20 - 30 seconds    |
| Volume (single shot)        | 30ml               |
| Doseage for **single** shot | 7 - 9 g (1 scoop)  |
| Doseage for **double** shot | 16-18 g (2 scoops) |

### Serving Criteria

- Be served immediately after brewing
- Warm the cup with hot water before filling espresso

Those are the possible reasons if a customer complains the espresso is not hot enough.

### Troubleshoot
If a 30ml espresso is extracted <20 / >30 seconds:
- Finer / croaser the **grind size**
- Up / down the **dose**
- Harder / lighter the **tamping force**

Note: you should try to adjust the grind size first and then the dosage (assuming that you are tamping the grinds correctly)

## Cappuccino & Caffe Latte

They are **milk** coffee with espresso as the base. Apart from them. **flat white** is also a coffee with milk and espresso.

|                        | Cappuccino         | Caffe Latte          |
| ---------------------- | ------------------ | -------------------- |
| (milk foam) Appearance | Fine and Smooth    | Fine and Smooth      |
| (milk foam) Thickness  | Thicker (1.5-3 cm) | Thinner (0.5-1.5 cm) |
| Temperature            | 55-70˚C            | 58-70˚C              |
| Espresso doseage       | 60ml (2 shots)     | 30ml (1 shot)        |

### Milk and Milk Foam

- Usually we use **fresh** (4˚C in refrigerator), **whole milk** (fat content ~3.5%)

- Use steam wand of the espresso machine to make milk foam from fresh milk.

- Temperature of the milk should within 60˚C - 70˚C (Perfect is ~65˚C)

  - if < 60˚C will have food hygine problem
  - if > 70˚C milk will be overheated and will have a brunt taste. (Protein will denature)

#### Troubleshoot

If the espresso and steam wand is function normally but you cannot make a good milk foam:

- Milk is not fresh (i.e. 4˚C, get it from the refrigerator);
- Milk has been steamed before

If liquid is splitted when making the coffee, you should clean up **immediately**

- Keep the workplace hygienic;
- Establish a positive image of the shop;
- Avoid accidents happen on staff or customers.

## Espresso machine

Main parts of a espresso machine:

![Diagram of the machine parts](https://www.espressoguy.com/pics/parts_diagram.gif)

- Inlet pipe (of filtered water)
- Boiler
- Steam wand
- Grouphead (w/ grouphead gasket)
- Portafilter (w/ filter basket)
- Drip tray

![Espresso machine parts](https://yoursilverservice.files.wordpress.com/2014/06/espresso-parts2.png)



### Preparation

#### Steam wand

![steam wand](https://espressoplanet.r.worldssl.net/images/P/10-049-046.jpg)

You should do the following clean up process before and after using the steam wand:

- Use a clean towel to rub and clean the outside
- Surround the tip using the clean towel
- Switch on the steam control to clean and remove the leftover water inside the wand
- Take off the towel

You should not soak the steam wand tip in water for too long

- Water may sucked into the boiler and thus pollute the clean water inside the boiler

#### Portafilter

![portafilters with different baskets](http://www.home-barista.com/la-spaziale-vivaldi-ii-review_files/pfs.jpg)

How to install the portafilter:

- Hold the group handle such that the end of the handle is pointing to ~8'o clock.
- Keep the filter basket just below the grouphead and stays parallel.
- Install the portafilter to grouphead, rotate the group handle anti-clockwisely.

Group handle should point to ~6'o clock and portafilter should be held by the group head for a successful installation.

### Cleaning

#### Backflush

![Backflush instructions](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcoffeebeansdelivered.com.au%2Fwp-content%2Fuploads%2F2018%2F06%2FEvo_EspreccoClean-1.png&f=1&nofb=1)

We should **backflush** the espresso **periodically** to:

- Deep clean the **grouphead**
- Remove the remaining coffee grinds in the **grouphead**

Note: backflush is all about the **grouphead**, not the portafilter.

### Troubleshoot

1.  Cannot use the steam wand to produce milk foam from fresh, unsteamed milk:
    - Check if the tip is blocked
    - Check if the steam pressure is high enough (~1.2-1.5 Bars)
2.  Water stains appears at the hot water outlet:
    - Clean the boiler and water outlet to remove stain
    - Might need to replace the filter of the water filter
3.  Water leaks from the bottom of the machine
    - Check if the drip tary is installed properly
    - Check if the water inlet pipe is connected properly
4.  Water leaks from the portafilter when brewing espresso
    - The portafilter might installed wrongly
    - The **grouphead gasket** might need to be replaced ![grouphead gasket](https://truestonecoffee.com/wp-content/uploads/2018/03/new-gasket.jpg)
    - Too much coffee grinds filled into the portafilter


## Drip Coffee

- **Filter coffee** can stay max. 1 hour in the coffee warmer. Coffee will taste worse after 1 hour.
- Compare with espresso, drip coffee (or french press) use a croarser grind size:
  - Croaser the grind size --> Longer the brew time
  - Finer the grind size --> Shorter the brew time
- Compare with espresso, filter coffee will taste less instense

### Brew ratio

When using a standard water : grinds ratio, you can:

- maintain the same **quality** & **taste**
- maintain a consistant **extraction**

![coffee extraction ratio](https://voltagecoffee.com/wp-content/uploads/2020/08/coffee-brewing-extraction-chart.png)

### Practical exam recipe

(see [recipe section at practical exam](../../exams/practical-exam#filter-coffee))

### Filter pan outflowing

In a good filter coffee brewing, water goes through the bottom hole should faster then water filling in the filter. If water stuck into the filter the water level will raise and cause **filter pan outflowing**. This may due to:

- Using a e.g stainless steel filter which the holes are blocked
- Using >1 filter paper when brewing filter coffee
- Grind size of the coffee is too fine
- Improper pouring time control

## Other Appliances

### Manual/Automatic Espresso Machine

We need to clean the parts **filling with milk** after steaming the milk:

- Disassemble the parts filling with milk
- Clean the parts throughtly everyday
- Assemble the parts properly after cleaning

### Thermometer

Useful to measure the temperature (of the liquid/drinks) in the coffee shop.