# On Smoothies

The most challenging part when making smoothies is **safety**.

## Electric Blender

Usually 20-30 seconds is enough to blend all the ingredients. However, **the time required may vary due to the power of each blender is different**.

When you see there is no ice cubes and the ingredients are well-mixed to form a smooth paste, it means the smoothies is ready.

![commercial blender](https://5.imimg.com/data5/ST/PG/PA/SELLER-8388917/41xupmy4gul-500x500.jpg)

### Safety Precautions

- Make sure the blender **is off** and motor **stops spinning** before touching the jug
- Take the jug **out of the motor** and place it on the desk before opening the lid
- Only food can be placed in the jug
- Some blenders have safety sensors. Motor won't operate if the jug is not placed at the correct position
- Lid should be covered when the blender is operating
- You should not touch the food directly e.g. use a spoon to handle

### Troubleshoot

1. If water leaks from the blender:
   - Check if the **lid** has been closed properly
   - Check if wear or tear on the **plastic ring** on the lid
2. If the machine doesn't start after switching on the power button
   - Check if the blender jug is placed properly on the motor
   - Some blenders won't work if the jug is not in a correct position
3. The blender is too noisy when operating
   - Use a soundproof cover to reduce the noise (especially for commercial blender)

## Ingredients

Smoothies contains fruit / other natural ingredients so it is rich in fibers can be treated as a source of daily fiber intake. It is considered as a healthy drink.

- Main ingredient: **ice**.

- (Frozen) Fruits / (concentrated) juice: **fill water** first when using (concentrated) juice
- Milk / Yogurt
- other natural ingredients

#### Storage

- Store the ingredents under **suitable temperature**
- If the container has a lid, close the lid tightly
- Store the ingredient with airtight containers when possible

## Recipe

### The pratical exam recipe

(see the [recipe section in practical exam](../../exams/practical-exam#smoothies))

### Standard Recipe

Follow the recipe used in your coffee shop. There is no right/wrong on the ingredients but you should:

- Keep a **consistent quality & taste**
