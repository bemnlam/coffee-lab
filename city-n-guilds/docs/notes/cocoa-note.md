# On Cocoa (Chocolate)

## Planting areas

Similar to coffee planting areas. Here are some countries:

- Mexico
- Indonesia
- Barzil
- Madagascar

## Harvesting

After the cocoa are harvested by farmers:

- Unpulp
- Fermentation (at least 3 rounds)
- Cleaned
- Sun dried

![Unripe and ripe cacao pods](https://perfectdailygrind.com/wp-content/uploads/2020/04/cacao14-1024x686.jpg)

### Fermentation steps [^1]	


(FYI only, not in the written exam)

Fermentation gives flavour to cocoa. However, fermentation is not happened at the beans [^2]:

> Rather, yeasts, bacteria, and enzymes ferment the juicy white pulp, or *baba*, that surrounds the cacao beans.

| Round | Time                      |
| ----- | ------------------------- |
| 1st   | 16 hours (Day 0-1)        |
| 2nd   | 48 hours (Day 1-3)        |
| 3rd+  | every 24 hours (Day 3-6+) |

## Manufacturing

After the cocoa beans are dried, beans will be sent to cocoa manufacturing facilities:

- 🔴 **R**oasted
- 🟢 **G**rounded and Liquified to chocolate liquor
- 🔵 **B**lended to make chocolate/cocoa powder

## Storage

Cocoa powder should be stored:

- Dry environment
- Cool environment
- Places/containers that don't have strong scent

## Chocolate Machine

![Chocolate matchine](https://itempics-tigerchef.netdna-ssl.com/Buffet-Enhancements-1BHC235-Chocolate-Shot-Drinking-Chocolate-Machine-in-Silver-140842_xlarge.jpg)

Chocolate machine can produce **chocolate milk foam**.

### Cleaning

You have to clean the **mixing accessories** of the chocolate machine **daily** in order to avoid hygienic problem.

### Troubleshoot

If the chocolate machine cannot produce chocolate milk foam properly, you should check:

- whipper **motor**
- whipper **paddle**

## Hot Chocolate Recipe

### The Practical Exam Recipe

(see [recipe section in practical exam](../../exams/practical-exam#hot-chocolate))

### Standard Recipe

Follow the **powder to milk ratio** provided by the **cocoa powder manufacturer**:

- standard quality & taste
- consistent **concentration**

You may also to increase the value of the drink by:

- using a tall, transparent cup
- decorating the drink with other ingredients e.g. marshmallow / cream


---

[^1]: [A Step-by-Step Explanation of Cacao Harvesting & Processing (perfectdailygrind.com)](https://perfectdailygrind.com/2018/02/step-step-explanation-cacao-harvesting-processing/)
[^2]: [What Happens During Cacao Fermentation? (perfectdailygrind.com)](https://perfectdailygrind.com/2019/06/what-happens-during-cacao-fermentation/)