# On Tea

Tea leaves is the common name of the species **Camellia Sinensis**. The common tea types are:

- ⚫️ Black tea
- ⚪️ White tea
- 🟠 Oolong tea
- 🟢 Green tea
- 💐 Herbal tea (no tea leaves)

## Tea planting

### Criteria

The flavour of teas is affected by:

- S: Soil
- C: Climate
- A: Altitude

### Planting areas

- Assam
- Darjeeling
- Ceylon / Sri Lanka

## Brewing Criteria

98.5˚C. If water temperature is too low (e.g. ≤75˚C):

- Weak in taste (due to under-extraction)
- A thin white layer might appear on the surface

## Comparison of tea

| Type          | Remarks                                       |      |
| ------------- | --------------------------------------------- | ---- |
| Green tea     | slightly bitter/**astringent**, softer, fresh |      |
| Black tea     | **stronger** flavour                          |      |
| Earl Gray tea | flavoured with **bergamot oil**               |      |
| Herbal tea    | 0% caffeine (no tea leaves)                   |      |

## Storage

- Airtight container
- Dry environment
- Cool environment
- Don't store tea leaves at the place/container with strong scent

## Recipe

(see [recipe section in practical exam](../../exams/practical-exam/#tea))