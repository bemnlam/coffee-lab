# Written exam example questions and answers

??? hint "This is a dummy question. Click me to reveal the answer"
    Here is the answer.

??? question "1. Why selective picking is the best coffee harvesting way?"
    Coffee farmers cna **judge** when is the best havesting time to pick the ripe coffee / cherries and won't pick the unripe / overripe / rotten cherries.

??? question "2. Explain 2 difference in appearance between Robusta and Arabica."
    Robusta is **round** and smaller while Arabica is **oval** and **larger**.

??? question "3. What is the ratio of fruit and other ingredient in a smoothies?"
    The ratio should follow the **standard recipe** in order to make the drink with **consistent quality & taste**.

??? question "4. How long does it take to blend a smoothies?"
    Depends on the power of the blender. There should not be noticible ice cubes remain in the smoothies and the ice should melt immediately in mouth.

??? question "5. Name 1 payment method in a coffee shop."
    Cash.

??? question "6. What happen if you handle customer's camplain properly?"
    The customer might return to the shop next time.

??? question "7. What should be the perfect texture of the Cappuccino & Caffee Latte?"
    The thickness of the milk foam layer should be **1.5-3 cm for a cappuccino** while **0.5-1.5 cm for a caffee latte**.

??? question "8. How much coffee grinds required to make 1 shot of coffee using a double basket?"
    16-18g. *(doseage depends on the size of the basket)*

??? question "9. What equipment should check if water leaks from the portafilter when brewing an espresso?"
    The **grouphead gasket** might need to be replaced.

??? question "10. What is the purpose of blackflushing a espresso machine?"
    To remove remaining coffee grinds and **deep clean** the **grouphead**.

??? question "11. If a customer camplains the espresso you serve is not hot enough, what will be the reason?"
    Didn't warm the espresso cup before filling espresso.

??? question "12. How to store chocolate properly?"
    Store in a **cool place**. *(Main focus is to avoid melting)*

??? question "13. What is the main difference in taste between green tea and black tea?"
    Green tea is slightly **bitter/astringent** then black tea.

??? question "14. What kind of flavouring oil is added to Earl Grey tea?"
    Bergamot oil.

??? question "15. Name 1 reason why coffee beans should be stored in bag with one-way valve."
    CO<sub>2</sub> can escape from the bag so that the bag will not explode.

??? question "16. How to prevent oxidation of coffee beans?"
    Grind the coffee beans just right before use.

??? question "17. What should you do if the blender does not working after switching on the power button?"
    Check if the blender jug is placed in a **correct position**.

??? question "18. How can the coffee shop reduce the noise from a blender?"
    Use a **soundproof cover**.

??? question "19. What happen when roasted coffee bean exposed to air?"
    Oxidation will happen. This causes the **flavour and aroma fade away** and the bean quality drops.

??? question "20. Why we need to keep a standard ratio of water and coffee grinds when making a filter coffee?"
    A standard water to coffee grinds ratio can produce a **standard quallity and taste**. Also, it can keep a **consistent extraction**.
