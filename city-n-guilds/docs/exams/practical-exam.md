# Practical Exam

The baseline of the exam is complete **7 drinks within 16 minutes**

- If you can't serve 7 drinks to the **examiner's desk**, you will failed.
- If you complete the task ≥16 minutes, you will failed.

You will have an extra **10 minutes** to do preparation / setup for the exam.

## The marking scheme

Full mark of the exam is 100 and it is divided into 4 areas:

|      Area |    Max. |   Min. |
| --------: | ------: | -----: |
|     Setup |      12 |      7 |
|    Drinks |      45 |     35 |
|     Speed |      15 |      5 |
|   Service |      28 |     14 |
| **Total** | **100** | **61** |

### Setup

You need to complete the followings within 10 minutes:

- Prepare all the necesary materials for the practical exam (on a serving tray)
- Check the coffee grinders settings (espresso and filter coffee)

#### Serving tray

Make sure that you prepare all the required materials (except the ice and cups for hot drinks). 

!!! Warning
	In order to keep good **food hygiene**, place the spoons / sugars / filter in small stainless steel dishes. **Do not place them directly on the tray.**
	
Here are the suggested list of preparation materials (scroll horizontally to see all the items):

| Item | Sugar | Pitcher | Milk    | Spoon  | Plate | Food   |
| ---------: | :-------: | ----------: | ----------: | :------ | ------ | ------ |
| Espresso | 🟤         | ---         | ---         | ✅[^1]  | ✅[^2] | --- |
| Latte    | 🟤         | 600ml       | 230ml       | ✅       | ✅ | --- |
| Capp. | 🟤         | --- | --- | ✅✅[^3] | ✅ | --- |
| Filter     | 🟤         | --- | --- | ✅       | ✅ | 10g beans |
| Chocolate | --- | 350ml       | 120ml       | ✅✅[^4] | ✅ | 2🥄cocoa |
| Tea        | ⚪️         | --- | --- | ✅       | ✅ | 1 teabag |
| Smoothies | 15ml [^5] | ≥45ml [^6]   | 45ml        | ✅✅[^7][^8] | --- | Ice, 1/3 🍌 |

*[Capp]: Pitcher and milk also share with latte.

#### Coffee grinders

In normal case the grinder settings are fixed so you don't have to take care of it. However, this may not be the same across all the examination centers.

For filter coffee, you have to **wet the filter** on the dripper before placing the coffee grinds (1 mark).

## Recipes

### Tea

Serving portion: 1cup (150ml)

| Ingredient | Requirement             |
| ---------- | ----------------------- |
| Tea        | 1 tea bag               |
| Water      | Hot water (e.g. 98.5˚C) |
| Sugar      | **White** sugar         |

- Place the tea bag into the teapot and pour the hot water
- Wait for **at least 4mins**
- Pour the tea out and serve with sugar and spoon

### Filter coffee

Serving portion: 1cup (150ml)

| Content              | Requirement     |
| -------------------- | --------------- |
| Grinds               | 10g             |
| Volume               | 150ml           |
| Time                 | 1:30            |
| Starting temperature | ~92˚C           |
| Sugar                | **Brown** Sugar |

| Time                | Water | Total | Remarks  |
| ------------------- | ----- | ----- | -------- |
| 00:00 - 00:10 (10s) | 20ml  | 20ml  | 1st pour |
| 00:10 - 00:30 (20s) | ---   | ---   | Resting  |
| 00:30 - 01:30 (60s) | 130ml | 150ml | 2nd pour |

- You may illustrate the bean you chose and expected flavour / taste to examiner during the resting phase
- You should wet the filter paper and pre-heat the coffee server before brewing
- Dispose the filter paper immediately after brewing

### Smoothies

| Ingredient                                                   | Requirement |
| ------------------------------------------------------------ | ----------- |
| Ice                                                          | 2/3 cup     |
| Banana                                                       | 1/3 slice   |
| Milk                                                         | 45ml (cold) |
| Syrup ([Monin french vanilla](https://www.monin.com/us/french-vanilla-syrup.html)) | 15ml        |

- Put the jug on the table before you open the lid and put your food in the jug
- 1 spoon for the customer, 1 spoon for yourself to handle the ingredients.
- Do not touch the food directly: use spoon

### Hot Chocolate

Serving portion: 1 cup (150ml)

| Ingredient   | Requirement            |
| ------------ | ---------------------- |
| Cocoa powder | 5g (2 flat tea spoons) |
| Hot water    | ~92˚C, 30ml            |
| Milk         | ~65˚C, 120ml (±10ml)   |

- Place the powder into the cup and fill the cup with hot water. Mix the powder with water using a spoon
- Warm the milk using the steam wand
- **No milk foam should be produced** when steaming
- It's not necessary to warm the cup in advance
- If we replaced the cocoa powder and hot water with an single-shot espresso, it will be a **flat white**
- You don't need to serve the cup with sugar (cocoa is already sweet enough)

### Espresso

Serving portion: 30ml (±5ml)

Extraction time: 21-29 seconds

Serve the espresso using the cup, dish and spoon especially for espresso. You may also serve with a **brown sugar**.

### Cappuccino

Serving portion: 150ml (with 60ml i.e. 2 shots of espresso)

The thickness of the milk foam layer should be 1.5-3cm and the remaining space should be filled with steamed milk.

Serve with brown sugar.

### Latte

Serving portion: 150ml  (with 60ml i.e. 1 shot of espresso)

The thickness of the milk foam layer should be 0.5-1.5cm the remaining space should be filled up with steamed milk.

Serve with brown sugar.

## Time Management

You should aim for finishing all the drinks using ~15 minutes. 12 is the best but it's not possible to make all the drinks within 8 minutes (if you are not a professional).

I personally splitted 7 drinks into 3 groups:

1. The easy: tea + filter coffee **(5 min)**
2. The milky: smoothies + hot chocolate **(5 min)**
3. The espresso: espresso + cappuccino + latte **(6 min)**

Depends on the set up of the exam room, the groups are interchangable.

## Useful Tips

### The easy

- You should notice that when you brewing the tea, you can do other stuff. For me I will use the brewing time to:
  1. make the filter coffee
  2. place all the smoothies ingredients into the jug

- Make sure that you wet the filter paper before and start the timer of the scale. You can use the resting time to describe the taste/flavour/body of the coffee you chose.

### The milky

#### Smoothies

- Take off the blender jug and **place it on the desk** before you start making the smoothies.
- Observe the ice cubes inside jug when the blender is operating. Power off the blender when you can't see any  ice cubes.
- Take off the jug when the motor is **stop spinning completely**. Place it on the desk before open the lid.
- Serve the tea when the blender is operating. (30 seconds is enough for you to serve the tea)

#### Hot chocolate

- Use a spoon to mix 30ml hot water with the powder before heating the milk.
- Bury the steam wand tip in the milk (prevent forthing)

### The espresso 

> Brew the espresso, cuppuccino and latte at once. If you cannot brew the coffee of these 3 drinks correctly it's nearly impossible to get 35 marks in the drink section.

The time limit for this part is strict: **6 minutes**. If you are confident to brew the drinks ≤ 4.5 minutes, you may consider to knock off the coffee grinds right after serving the espresso.

- Arrange the cups (1 measuring glass + 1 espresso cup + 2 cups) when the grinder is operating.
- Wash the groupheads before you install the portafilters.
- Place the cups at right position **after** you start pressing the brewing buttons.
- Observe the measuring glass at with your eye level stays **horizontally** with the 30ml marks.
- Stop brewing when timer of the machine is within **21-29** seconds (avoid 1-sec delay of the timer and cause your brewing time is not within 20-30 seconds).
- Serve the espresso immediately (within 30 second after the brew).
- Brew the cappuccino first and then the latte.

#### Machine setup

In order to brew all the drinks within the time limit, you have to utilize **both grounpheads** of the espresso machine.

![Espresso Setup.001](practical-exam.assets/Espresso%20Setup.001.jpeg)

- Press the brew button of both grouphead **at the same time**.
- Use the measuring glass to check if the volume reach 30ml (±5ml).
- Use hot water to preheat all the cups. Place the latte cup on the desk and keep the hot water until you decide to pour the coffee from the measuring glass.

#### Pitcher

Steam the milk and make the milk foam after you brew and serve the espresso. Make sure that the milk is **within 60˚C-70˚C**.

#### Cappuccino

Add the foam to the cup using the spoon. When you got enough milk foam, pour the steamed milk to the cup and **place the spoon at the mouth of the pitcher** in order to stop extra foam pouring into the cup.

You need to make the drink is filled up the cup and covers the rim of the cup. Add some choco powder on the top before serving the cup.

The temperature of the final cup should be **within 55-70˚C**.

#### Latte

It's not a must to create latte art during the exam. However, you must make sure that the thickness of the milk foam layer is within the acceptable range.

Similar to cappuccino, the coffee should covers the rim of the cup.

The temperature of the final cup should be **within 58-70˚C**.

### Gantt Chart

Scroll horizontally to see the timeline:

<div style="width:100%;overflow-x:scroll;">
<img style="max-width: inherit;max-height:500px" src="{{ config.site_url }}assets/gantt-chart-2.png"/>
</div>

[![Expected time management]({{ config.site_url }}assets/gantt-chart-2.png)]({{ config.site_url }}assets/gantt-chart-2.png)


[^1]: **Small** espresso spoon (smaller surface to reduce heat loss)
[^2]: **Small** espresso serving plate (fits the cup size)
[^3]: Blocking the milk foam when pouring milk form pitcher
[^4]: 150ml glass cup
[^5]: Vanilla syrup in a 45ml measuring cup
[^6]: Approx. 4/5 measuring cup / small milk cup
[^7]: **Long** spoon for the tall cup
[^8]: **Long and big** spoon to cut banana and pour smoothies from the jug