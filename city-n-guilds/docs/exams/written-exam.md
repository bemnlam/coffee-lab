# Written Exam

You need to answer 20 short questions within 20 minutes. You need to answer 60% of the questions correctly (i.e. 12).

| Marks | Grade       |
| ----- | ----------- |
| <12   | Fail        |
| 12-17 | Merit       |
| 18-20 | Distinction |

## Skills

Here are some skills to answer the questions.

### "Main"

Some questions will ask you the *main* reason/difference. In this case, remember to answer the most critical point.

???+ question "What is the main difference in taste between green tea and black tea."
    Green tea is **slightly bitter/astringent** then black tea.

Although green tea is also softer and fresher then black tea, they are **not the main difference**.

### "Ratio"

Not all the drinks (indeed coffee is the only exception) has a standard ingredient *ratio*. However, you still need to point out the *purpose of having the same recipe* for the same drink.

???+ question "What should be the ratio of the fruit and other ingredients in a smoothies?"
    The ratio should follow the **standard recipe** in order to make the drink with **consistent quality & taste**

Remember, **consistent** quality & taste/concentration is the reason why we need to have a recipe / ratio. Not all the drinks will have a numeric formula like expresso or filter coffee.

### "Basket" vs "Shot"

You might see those terms when answering questions related to the expresso machine. **Basket** controls the amount of coffee grinds (**input**) while **shot** controls the serving volume (**output**).

???+ question "How many coffee grinds is required when brewing with double basket in 1 shot?"
    16 - 18 grams.

The question is asking the **input** of the expresso. Therefore you should focus on the keyword **double basket** (i.e. 2 scoops)


