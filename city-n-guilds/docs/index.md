# Introduction

**City & Guilds International Barista Skills (7102)** is for a person who works as or wants to work as barista. You don't have any experience on this field before taking this course.

_If you want to see my notes on coffee, please visit [my blog](https://blog.lofibean.cc/categories/coffee/?from=coffeelab-intro) as well._

## The exams

You will become a qualified barista if you pass the both exams:
1. Written exam: 20 questions, 20 short answers in 20 minutes.
2. Practical exam: 7 drinks within 16 minutes.

You only have to re-take the failed part and you can re-take at most **twice**.

## The course

My expreience is having a 3-day course, having the written part at the end of 2nd day and the partical exam at the end of the 3rd day. It's really demanding but luckily I had a 3-day annual leave so that's why I took it in this way.

If your work is already exhausted you, you may consider to have a weekend/nighttime course and this will take you around 1 month. Plus, you can have some extra time for studying.

### Content of the course

More then 50% of the course is related to coffee (machines) while the other half is related to the other drinks and the skills required for a barista.

The main focus of the coffee part is the **Espresso**:

#### Coffee

- Ingredient of a good coffee
- From bean to cup
- How to delivery a good espresso
- How to brew good espresso-base coffee
- Storage
- Troubleshoot / maintenance of an espresso machine

The other drinks covers **tea**, hot **chocolate** and **smoothies**:

#### Other drinks

1. Tea:
    - Types of tea
    - How to brew good tea
    - Storage

2. Chocolate:
    - From cocoa to hot chocolate
    - Troubleshoot on chocolate machine
    - Storage

3. Smoothies: 
    - Ingredients
    - How to use/troubleshoot a blender

#### Others
Last part is the soft skills including:

- Communication skills
- What to do when your shop does not have filter coffee
