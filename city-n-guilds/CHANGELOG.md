# Changelog

## [1.0.4] 2022-02-12
- Move to custom domain https://coffeelab.lofibean.cc.

## [1.0.3] 2021-02-13
- Move to custom domain https://coffeelab.eternalrecurrence.space/.

## [1.0.2] 2021-01-22
- Fix webmanifest url.
- Add og tags and default cover image.

## [1.0.1] 2021-01-01
- Fix GA config.
- Add site search.
- Add external links in footer.
- Add practical exam recipes and tips.

## [1.0.0] 2020-12-30
- Init setup the site: https://bemnlam.gitlab.io/coffee-lab/.

_(end of changelog)_