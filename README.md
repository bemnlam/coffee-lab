This is a static websites contains notes for my Barista exam. See https://bemnlam.gitlab.io/coffee-lab/about/ for details.

## Up and Run

### Local Development

```bash
cd ./city-n-guilds
mkdocs serve
```

Go to http://127.0.0.1:8000

### Deployment

See [./.gitlab-ci.yml](./.gitlab-ci.yml). CI/CD pipeline will run when new code is merged to `master`.